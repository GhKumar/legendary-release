/*
Class   : LSC_ConsolidatedCompController
Author  : Extentia
Details : This class is used to parse the JSON of objects and fields and return a list worth rendering.
Coverage Classes: LSC_ConsolidatedCompController_Test
History : v1.0 - 27/03/2019 - Creation (Code Coverage - 96%)
*/

public with sharing class LSC_ConsolidatedCompController
{
    @AuraEnabled
    public static List<Object> GetListOfFields(String staticResName, String SobjectType)
    {
        String staticResNameToQuery = 'TEST_JSON';
        
        if(String.isNotBlank(staticResName) && !staticResNameToQuery.equals(staticResName))
        {
            staticResNameToQuery = staticResName;
        }
        
        //Query the static resource containing JSON
        StaticResource srObject = null;
        Try
        {
            srObject = [select id,body from StaticResource Where Name = :staticResNameToQuery];
        }
        Catch(exception ex)
        {
            System.debug(ex.getStackTraceString());
            return null;
        }
        
        //Convert the static resource to a string
        String staticResAsString = srObject.body.toString();
        
        //For test class
        if(Test.isRunningTest())
        {
            staticResAsString = LSC_ConsolidatedCompController_Test.testJSONstr;
        }
        
        //Get a map of object name with list of it's fields from the JSON
        Map<String,Object> mapObjFieldInfo = new Map<String,Object>();
        mapObjFieldInfo = (Map<String,Object>)JSON.deserializeUntyped(staticResAsString);
        
        List<Object> lstToRender = new List<Object>();
        
        if(mapObjFieldInfo!=null && mapObjFieldInfo.size()>0)
        {
            Map<String,Object> mapFields;
            List<Object> lstFieldProperties;
            if(String.isNotBlank(SobjectType))
            {
                mapFields = new Map<String,Object>();
                mapFields = (Map<String,Object>)mapObjFieldInfo.get(SObjectType);
                lstFieldProperties = new List<Object>();
                lstFieldProperties = GetMapOfFieldsForObject(SObjectType, (List<Object>)mapFields.get('fieldsToRender'));
                lstToRender.add(lstFieldProperties);
            }
            else
            {
                for(String objName: mapObjFieldInfo.keySet())
                {
                    mapFields = new Map<String,Object>();
                    mapFields = (Map<String,Object>)mapObjFieldInfo.get(objName);
                    lstFieldProperties = new List<Object>();
                    lstFieldProperties = GetMapOfFieldsForObject(objName, (List<Object>)mapFields.get('fieldsToRender'));
                    lstToRender.add(lstFieldProperties);
                }
            }
        }
        
        return lstToRender;
    }
    
    private static List<Object> GetMapOfFieldsForObject(String objectAPIName, List<Object> lstFieldInfo)
    {
        //Mapping of data types received using describe calls to the type used on the lightning input component
        Map<String, String> mapFieldInputTypes = new Map<String, String>
        {
            	'STRING'=>'text',
                'DOUBLE'=>'number',
                'INTEGER'=>'number',
                'EMAIL'=>'email',
                'PERCENT'=>'number', 
                'CURRENCY'=>'number',
                'DATE'=>'date',
                'DATETIME'=>'datetime',
                'URL'=>'url',
                'PHONE'=>'tel'
                };
		List<Map<String,Object>> lstFieldsToRender = new List<Map<String,Object>>();
        Schema.SObjectType sObj = Schema.getGlobalDescribe().get(objectAPIName);
        Map<String, Schema.SObjectField> mapFields = sObj.getDescribe().fields.getMap();
        
        for(Object fieldInfo: lstFieldInfo)
        {
            Map<String,Object> mapFieldProperties = new Map<String,Object>();
            mapFieldProperties = (Map<String,Object>)fieldInfo;
            String fieldAPIName = (String)mapFieldProperties.get('fieldAPIName');
            Schema.DescribeFieldResult desribeResult = mapFields.get(fieldAPIName).getDescribe();
            
            if(!LSC_AccessUtil.getFieldReadAccessCheck(objectAPIName, fieldAPIName))
            {
                continue;
            }
            
            mapFieldProperties.put('objectAPIName', String.valueOf(objectAPIName));
            mapFieldProperties.put('fieldAPIName', fieldAPIName);
            mapFieldProperties.put('isOfInputType', FALSE);
            
            //fieldState is TRUE if field is editable.
            Boolean fieldState;
            if(!LSC_AccessUtil.getFieldCreateAccessCheck(objectAPIName, fieldAPIName) && !LSC_AccessUtil.getFieldEditAccessCheck(objectAPIName, fieldAPIName))
            {
                fieldState = FALSE;
            }
            else
            {
                fieldState = ((String)mapFieldProperties.get('stateOFTheField')).equalsIgnoreCase('Input')?TRUE:FALSE;
            }
            mapFieldProperties.put('stateOfField', !fieldState);
            
            mapFieldProperties.put('isRequired', FALSE);
            //desribeResult.isNillable() returns true if field can be kept blank, false otherwise
            if((desribeResult.isNillable()==FALSE) || (desribeResult.isNillable()==TRUE && String.isNotBlank((String)mapFieldProperties.get('required')) && ((String)mapFieldProperties.get('required')).equalsIgnoreCase('true')))
            {
                mapFieldProperties.put('isRequired', TRUE);
            }
            else if(String.isBlank((String)mapFieldProperties.get('required')))
            {
                mapFieldProperties.put('isRequired', !desribeResult.isNillable());
            }
            
            if(desribeResult.getType() == Schema.DisplayType.TEXTAREA && desribeResult.isHtmlFormatted())
            {
                mapFieldProperties.put('fieldType', 'RICHTEXTAREA');
            }
            else
            {
                String fldType =  String.valueOf(desribeResult.getType());
                mapFieldProperties.put('fieldType', fldType);
                if(mapFieldInputTypes.containsKey(fldType))
                {
                    mapFieldProperties.put('isOfInputType', TRUE);
                    mapFieldProperties.put('fieldInputType', mapFieldInputTypes.get(fldType));
                    
                    if(fldType.equalsIgnoreCase('PERCENT'))
                    {
                        mapFieldProperties.put('formatter', 'percent');
                    }
                    else if(fldType.equalsIgnoreCase('CURRENCY'))
                    {
                        mapFieldProperties.put('formatter', 'currency');
                    }
                }
            }
            
            if(mapFieldProperties.containsKey('label') && String.isNotBlank((String)mapFieldProperties.get('label')))
            {
                mapFieldProperties.put('fieldLabel', (String)mapFieldProperties.get('label'));
                
            }
            else
            {
                mapFieldProperties.put('fieldLabel', desribeResult.getLabel());
            }
            
            if(String.valueOf(desribeResult.getType()) == 'PICKLIST' || String.valueOf(desribeResult.getType()) == 'MULTIPICKLIST')
            {
                List<String> pickListValues = new List<String>();
                for(Schema.PicklistEntry ple: desribeResult.getPicklistValues())
                {
                    pickListValues.add(ple.getLabel());
                }
                mapFieldProperties.put('pickListValues', pickListValues);
            }
            
            if(String.valueOf(desribeResult.getType()) == 'REFERENCE')
            {
                String str = String.valueOf(desribeResult.getReferenceTo()).removeStart('(');
                String strToPass =  str.removeEnd(')');
                mapFieldProperties.put('lookupObjectToSearch', strToPass);
            }
        }
        return lstFieldInfo;
    }
}