@isTest
public class LSC_ConsolidatedCompController_Test {
    
    //Static JSON string to be used for test coverage
    public static string testJSONstr = '{"Account":{"fieldsToRender":[{"stateOFTheField":"Input","fieldAPIName":"Name","label":"Name of the customer","required":""},{"stateOFTheField":"Input","fieldAPIName":"Phone","label":"","required":""},{"stateOFTheField":"Input","fieldAPIName":"Fax","label":"","required":""},{"stateOFTheField":"Input","fieldAPIName":"NumberOfEmployees","label":"Employee Count","required":""},{"stateOFTheField":"Input","fieldAPIName":"LSC1__Email__c","label":"Customer Email","required":"true"},{"stateOFTheField":"Output","fieldAPIName":"Site","label":"Customer Site","required":"true"},{"stateOFTheField":"Output","fieldAPIName":"LSC1__PO_Document__c","label":"Customer Document","required":"false"},{"stateOFTheField":"Input","fieldAPIName":"LSC1__Approved_Payer__c","label":"","required":""},{"stateOFTheField":"Output","fieldAPIName":"LSC1__Consignment_partner__c","label":"Customer Consignment Partner","required":"false"},{"stateOFTheField":"Input","fieldAPIName":"LSC1__Last_Statement_Issued_Date__c","label":"","required":"true"},{"stateOFTheField":"Input","fieldAPIName":"AccountSource","label":"Select source of this account","required":"true"},{"stateOFTheField":"Input","fieldAPIName":"Test_PleaseIgnore_AutoNum__c","label":"","required":"true"},{"stateOFTheField":"Input","fieldAPIName":"Test_PlzIgnore_Formula__c","label":"","required":"false"},{"stateOFTheField":"Input","fieldAPIName":"Test_plzIgnore_RollUp__c","label":"","required":"false"},{"stateOFTheField":"Input","fieldAPIName":"AnnualRevenue","label":"","required":"false"},{"stateOFTheField":"Input","fieldAPIName":"LSC1__Consignment_Percent__c","label":"","required":"false"},{"stateOFTheField":"Input","fieldAPIName":"Description","label":"","required":"false"},{"stateOFTheField":"Input","fieldAPIName":"Test_PleaseIgnore_RichText__c","label":"","required":"false"}]},"Contact":{"fieldsToRender":[{"stateOFTheField":"Input","fieldAPIName":"Name","label":"","required":"true"},{"stateOFTheField":"Input","fieldAPIName":"Phone","label":"","required":"true"},{"stateOFTheField":"Input","fieldAPIName":"AssistantName","label":"The Name of the assistant","required":"true"},{"stateOFTheField":"Input","fieldAPIName":"Birthdate","label":"","required":"false"},{"stateOFTheField":"Output","fieldAPIName":"Department","label":"","required":"true"},{"stateOFTheField":"Input","fieldAPIName":"LSC1__Contact_Status__c","label":"","required":"true"},{"stateOFTheField":"Input","fieldAPIName":"LastCUUpdateDate","label":"","required":"true"},{"stateOFTheField":"Input","fieldAPIName":"LSC1__Primary_Storage_Bin__c","label":"","required":"true"},{"stateOFTheField":"Input","fieldAPIName":"AssistantPhone","label":"","required":"true"}]}}';
    
    @isTest static void GetListOfFieldsTest_withoutSobject() {
        Test.startTest();
        List<Object> lstForAllObjects = LSC_ConsolidatedCompController.GetListOfFields('', '');
        System.assertEquals(lstForAllObjects.size(), 2);
        Test.stopTest();
    }
    
    @isTest static void GetListOfFieldsTest_withSobject() {
        Test.startTest();
        List<Object> lstForSobject = LSC_ConsolidatedCompController.GetListOfFields('', 'Account');
        System.assertEquals(lstForSobject.size(), 1);
        Test.stopTest();
    }
}