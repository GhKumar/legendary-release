/*
Class   : LSC_AccessUtil
Author  : Extentia
Details : This class is used to check the FLS of fields.
Coverage Classes: LSC_ConsolidatedCompController_Test
History : v1.0 - 27/03/2019 - Creation (Code Coverage - 93%)
*/

public class LSC_AccessUtil {
    public static Map<String, Schema.SObjectType> mapObjects = Schema.getGlobalDescribe();
    
    public static boolean getFieldReadAccessCheck(String objectName, String fieldToCheck)
    {
        Map<String, Schema.SObjectField> mapFields = mapObjects.get( objectName ).getDescribe().fields.getMap();
        if (mapFields.containsKey(fieldToCheck) && mapFields.get(fieldToCheck).getDescribe().isAccessible())
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public static boolean getFieldCreateAccessCheck(String objectName, String fieldToCheck)
    {
        Map<String, Schema.SObjectField> mapFields = mapObjects.get( objectName ).getDescribe().fields.getMap();
        if (mapFields.containsKey(fieldToCheck) && mapFields.get(fieldToCheck).getDescribe().isCreateable())
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public static boolean getFieldEditAccessCheck(String objectName, String fieldToCheck)
    {
        Map<String, Schema.SObjectField> mapFields = mapObjects.get( objectName ).getDescribe().fields.getMap();
        if (mapFields.containsKey(fieldToCheck) && mapFields.get(fieldToCheck).getDescribe().isUpdateable())
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}