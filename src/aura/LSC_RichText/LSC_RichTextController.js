({
    onValidate : function(component, event, helper) {
        var inputField = component.find('fieldId');
        var value = inputField.get('v.value');
        if($A.util.isEmpty(value) && component.get('v.required')){
            component.set('v.validity', false);
            component.set('v.errorMessage', 'Complete this field.');
            return false;
        }else if(!component.get('v.validity')){
            component.set('v.validity', true);
            component.set('v.errorMessage', '');  
            return true;
        }  
    }
})