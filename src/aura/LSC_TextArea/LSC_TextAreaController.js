({
    onValidate : function(component, event, helper) {
        var inputField = component.find('fieldId');
        var value = inputField.get('v.value');
        if($A.util.isEmpty(value)) {
            inputField.set('v.validity', {valid:false, badInput :true});
            inputField.showHelpMessageIfInvalid();
            return false;
        }
        return true;
    }
})