({
    getListOfFieldsToRender : function(component, event, helper) {
        var action = component.get("c.GetListOfFields");
        var staticResourceName =  component.get('v.staticResourceName');
        var objectName =  component.get('v.sobjecttype');
        action.setParams({
            "staticResName": staticResourceName,
            "SobjectType": objectName
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                component.set('v.listToRenderOnComp', result);
            }
            else if(state==='INCOMPLETE'){
                console.log('User is Offline System does not support drafts '
                            + JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action);
    },
    
    updateFieldsOnComponent : function(component, event, helper){
        var fields = component.get("v.listToRenderOnComp");
        var record = component.get("v.recordObject");
        for(var i = 0; i < fields.length; i++)
        {
            for(var j = 0; j < fields[i].length; j++)
            {
                {
                    fields[i][j].fieldValue = record[fields[i][j].fieldAPIName];
                    if(fields[i][j].fieldType=='MULTIPICKLIST')
                    {
                        if(record[fields[i][j].fieldAPIName])
                        {
                            fields[i][j].fieldValue = (record[fields[i][j].fieldAPIName]).split(';');
                        }
                        
                    }
                }
            }
        }
        component.set("v.listToRenderOnComp", fields);
    }
})