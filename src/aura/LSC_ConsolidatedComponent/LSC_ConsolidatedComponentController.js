({
    doInit : function(component, event, helper) {
        helper.getListOfFieldsToRender(component, event, helper);
    },
    updateFields : function(component, event, helper){
        helper.updateFieldsOnComponent(component, event, helper);
    }
})