({
    doInit: function(component, event, helper) {
        var picklistVals = component.get("v.pickListValues");
        var plValues = [];
        for (var i = 0; i < picklistVals.length; i++) {
            plValues.push({
                label: picklistVals[i],
                value: picklistVals[i]
            });
        }
        component.set("v.multipickListValues", plValues);
    },
    
    handleChange: function (component, event, helper) {
        //Get the Selected values  
        var selectedValues = event.getParam("value");
        
        //Update the Selected Values
        component.set("v.value", selectedValues);
    },
    
    onValidate : function(component, event, helper) {
        var inputField = component.find('fieldId');
        var value = inputField.get('v.value');
        if($A.util.isEmpty(value)) {
            inputField.set('v.validity', {valid:false, badInput :true});
            inputField.showHelpMessageIfInvalid();
            return false;
        }
        return true;
    }
})